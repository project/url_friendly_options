# URL-friendly options

This module enforces that all option list fields have keys that are composed
only of alpha-numeric characters and hyphens.

For example the list below is a valid list:

`first-value|First value`
`second-value|Second value`

While this list isn't allowed:

`first_value|First value`
`second value!|Second value!`

This means that if you end up needing to use this field as a contextual filter
in a view, the keys are already URL-friendly.

## Notes

1. This module will not change existing values. However, it won't be possible to
add new values to the list if there are non-compliant pre-existing keys, since
the form won't validate. A status report message will display an error if there
are any keys that fail validation.

2. If there are exceptions that need to bypass this module's validation,
developers can implement `hook_url_friendly_options_bypass_field_validation()`
and mark a particular field to be skipped.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers

- Marcos Cano - [marcoscano](https://www.drupal.org/u/marcoscano)